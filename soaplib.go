package soaplib

import (
	"bytes"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

var httpClient = &http.Client{
	Transport: &http.Transport{
		DisableKeepAlives:   false,
		MaxIdleConnsPerHost: 50,
	},
}

type SoapService struct {
	Service string
}

func NewService(path string, validation bool) (*SoapService, error) {
	if validation {
		u, err := url.Parse(path)
		if err != nil || u.Scheme == "" {
			return nil, errors.New(path + "is not a valid path")
		}
	}
	return &SoapService{path}, nil
}

func (ss *SoapService) PerformRequest(request string, outStruct interface{}) error {
	resp, err := httpClient.Post(ss.Service, "text/xml; charset=utf-8", bytes.NewBufferString(request))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	bresp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	in := string(bresp)
	//parser := xml.NewDecoder(bytes.NewBufferString(in))
	//fmt.Printf("outStruct type: %s\n", reflect.TypeOf(outStruct).String())
	//err = parser.DecodeElement(outStruct, nil)
	err = xml.Unmarshal([]byte(in), outStruct)
	if err != nil {
		return err
	}
	return nil
}
