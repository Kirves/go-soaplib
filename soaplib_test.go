package soaplib

import (
	"testing"
)

func TestValidPath(t *testing.T) {
	test_path := "http://wsgm.atm-mi.it/WebServicePaline.asmx?WSDL"
	ss, err := NewService(test_path, true)
	if err != nil {
		t.Error("NewService method return error: %s", err)
	}
	if ss.Service != test_path {
		t.Error("SoapService has a different path value")
	}
}

func TestInvalidPath(t *testing.T) {
	test_path := "an_invalid_path"
	_, err := NewService(test_path, true)
	if err == nil {
		t.Error("NewService method did not return error")
	}
	t.Logf("Error: %s", err)
}
